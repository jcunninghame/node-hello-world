var express = require('express');
var router = express.Router();

router
    .param('username', function (req, res, next, username) {
        req.user = username;
        next();
    })

    /* GET login page. */
    .get('/:username', function (req, res) {
        res.render('home', {user: req.user});
    });

module.exports = router;