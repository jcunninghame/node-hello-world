var express = require('express');
var router = express.Router();
var db = require("redis-js");

router

/* GET login page. */
    .get('/', function (req, res) {
        res.render('login');
    })

    /* POST login form */
    .post('/', function (req, res) {
        var user = req.body['username'];
        var password = req.body['password'];
        if (db.get(user) === password) {
            res.redirect('/home/' + user);
        } else {
            res.render('login', {error: true});
        }
    });

module.exports = router;