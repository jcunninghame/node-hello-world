var express = require('express');
var router = express.Router();
var db = require("redis-js");

router

/* GET users listing. */
    .get('/', function (req, res) {
        res.render('users', {users: db.keys()});
    })

    /* POST login form */
    .post('/', function (req, res) {
        var user = req.body['username'];
        var password = req.body['password'];
        if (db.get(user) == null) {
            db.set(user, password);
            res.redirect('/login');
        } else {
            res.render('users', {userexists: true});
        }
    });

module.exports = router;